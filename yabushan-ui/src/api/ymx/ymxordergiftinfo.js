import request from '@/utils/request'

// 查询订单礼物列表
export function listYmxordergiftinfo(query) {
  return request({
    url: '/ymx/ymxordergiftinfo/list',
    method: 'get',
    params: query
  })
}

// 查询订单礼物详细
export function getYmxordergiftinfo(orderGiftId) {
  return request({
    url: '/ymx/ymxordergiftinfo/' + orderGiftId,
    method: 'get'
  })
}

// 新增订单礼物
export function addYmxordergiftinfo(data) {
  return request({
    url: '/ymx/ymxordergiftinfo',
    method: 'post',
    data: data
  })
}

// 修改订单礼物
export function updateYmxordergiftinfo(data) {
  return request({
    url: '/ymx/ymxordergiftinfo',
    method: 'put',
    data: data
  })
}

// 删除订单礼物
export function delYmxordergiftinfo(orderGiftId) {
  return request({
    url: '/ymx/ymxordergiftinfo/' + orderGiftId,
    method: 'delete'
  })
}

// 导出订单礼物
export function exportYmxordergiftinfo(query) {
  return request({
    url: '/ymx/ymxordergiftinfo/export',
    method: 'get',
    params: query
  })
}