import request from '@/utils/request'

// 查询 动态表信息列表
export function listAutotable(query) {
  return request({
    url: '/form/autotable/list',
    method: 'get',
    params: query
  })
}

// 查询 动态表信息详细
export function getAutotable(bId) {
  return request({
    url: '/form/autotable/' + bId,
    method: 'get'
  })
}

// 新增 动态表信息
export function addAutotable(data) {
  return request({
    url: '/form/autotable',
    method: 'post',
    data: data
  })
}

// 修改 动态表信息
export function updateAutotable(data) {
  return request({
    url: '/form/autotable',
    method: 'put',
    data: data
  })
}

// 删除 动态表信息
export function delAutotable(bId) {
  return request({
    url: '/form/autotable/' + bId,
    method: 'delete'
  })
}

// 导出 动态表信息
export function exportAutotable(query) {
  return request({
    url: '/form/autotable/export',
    method: 'get',
    params: query
  })
}