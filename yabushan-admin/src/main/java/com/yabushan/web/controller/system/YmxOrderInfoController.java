package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxOrderInfo;
import com.yabushan.system.service.IYmxOrderInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 订单Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxorderinfo")
public class YmxOrderInfoController extends BaseController
{
    @Autowired
    private IYmxOrderInfoService ymxOrderInfoService;

    /**
     * 查询订单列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxorderinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxOrderInfo ymxOrderInfo)
    {
        startPage();
        List<YmxOrderInfo> list = ymxOrderInfoService.selectYmxOrderInfoList(ymxOrderInfo);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxorderinfo:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxOrderInfo ymxOrderInfo)
    {
        List<YmxOrderInfo> list = ymxOrderInfoService.selectYmxOrderInfoList(ymxOrderInfo);
        ExcelUtil<YmxOrderInfo> util = new ExcelUtil<YmxOrderInfo>(YmxOrderInfo.class);
        return util.exportExcel(list, "ymxorderinfo");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxorderinfo:query')")
    @GetMapping(value = "/{orderId}")
    public AjaxResult getInfo(@PathVariable("orderId") String orderId)
    {
        return AjaxResult.success(ymxOrderInfoService.selectYmxOrderInfoById(orderId));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxorderinfo:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxOrderInfo ymxOrderInfo)
    {
        return toAjax(ymxOrderInfoService.insertYmxOrderInfo(ymxOrderInfo));
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxorderinfo:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxOrderInfo ymxOrderInfo)
    {
        return toAjax(ymxOrderInfoService.updateYmxOrderInfo(ymxOrderInfo));
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxorderinfo:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderIds}")
    public AjaxResult remove(@PathVariable String[] orderIds)
    {
        return toAjax(ymxOrderInfoService.deleteYmxOrderInfoByIds(orderIds));
    }
}
