package com.yabushan.web.controller.system;

import java.util.List;

import com.yabushan.common.core.domain.entity.SysDept;
import com.yabushan.common.utils.StringUtils;
import com.yabushan.system.utils.OrgTreeSelect;
import com.yabushan.system.utils.UumOrgInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.UumOrganizationinfo;
import com.yabushan.system.service.IUumOrganizationinfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 租户组织Controller
 *
 * @author yabushan
 * @date 2021-04-17
 */
@RestController
@RequestMapping("/system/organizationinfo")
public class UumOrganizationinfoController extends BaseController
{
    @Autowired
    private IUumOrganizationinfoService uumOrganizationinfoService;

    /**
     * 查询租户组织列表
     */
    @PreAuthorize("@ss.hasPermi('system:organizationinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(UumOrganizationinfo uumOrganizationinfo)
    {
        startPage();
        //组织树列表查询接口
        if(StringUtils.isNotEmpty(uumOrganizationinfo.getOuguid())){
            UumOrganizationinfo uumOrganizationinfo1 = uumOrganizationinfoService.selectUumOrganizationinfoById(uumOrganizationinfo.getOuguid());
            if(uumOrganizationinfo1.getOuLevel()==1){
                //租户
                uumOrganizationinfo.setRegionKey(uumOrganizationinfo1.getRegionKey());

            }else if(uumOrganizationinfo1.getOuLevel()==2){
                //公司
                uumOrganizationinfo.setCompanyId(uumOrganizationinfo.getOuguid());
            }else if(uumOrganizationinfo1.getOuLevel()==3){
                //分公司
                uumOrganizationinfo.setBranchId(uumOrganizationinfo.getOuguid());
            }else if(uumOrganizationinfo1.getOuLevel()==4){
                //部门
                uumOrganizationinfo.setDepartmentId(uumOrganizationinfo.getOuguid());
            }else if(uumOrganizationinfo1.getOuLevel()==5){
                //科室
                uumOrganizationinfo.setOfficeId(uumOrganizationinfo.getOuguid());
            }else if(uumOrganizationinfo1.getOuLevel()==6){
                //团队
                uumOrganizationinfo.setTeamId(uumOrganizationinfo.getOuguid());
            }else if(uumOrganizationinfo1.getOuLevel()==7){
                //小组
                uumOrganizationinfo.setGroupId(uumOrganizationinfo.getOuguid());
            }
        }
        uumOrganizationinfo.setOuguid(uumOrganizationinfo.getOuguid());
        uumOrganizationinfo.setParentOuguid(uumOrganizationinfo.getOuguid());
        List<UumOrganizationinfo> list = uumOrganizationinfoService.selectUumOrganizationinfoList(uumOrganizationinfo);
        return getDataTable(list);
    }

    /**
     * 导出租户组织列表
     */
    @PreAuthorize("@ss.hasPermi('system:organizationinfo:export')")
    @Log(title = "租户组织", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UumOrganizationinfo uumOrganizationinfo)
    {
        List<UumOrganizationinfo> list = uumOrganizationinfoService.selectUumOrganizationinfoList(uumOrganizationinfo);
        ExcelUtil<UumOrganizationinfo> util = new ExcelUtil<UumOrganizationinfo>(UumOrganizationinfo.class);
        return util.exportExcel(list, "organizationinfo");
    }

    /**
     * 获取租户组织详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:organizationinfo:query')")
    @GetMapping(value = "/{ouguid}")
    public AjaxResult getInfo(@PathVariable("ouguid") String ouguid)
    {
        return AjaxResult.success(uumOrganizationinfoService.selectUumOrganizationinfoById(ouguid));
    }

    /**
     * 新增租户组织
     */
    @PreAuthorize("@ss.hasPermi('system:organizationinfo:add')")
    @Log(title = "租户组织", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UumOrganizationinfo uumOrganizationinfo)
    {
        return toAjax(uumOrganizationinfoService.insertUumOrganizationinfo(uumOrganizationinfo));
    }

    /**
     * 修改租户组织
     */
    @PreAuthorize("@ss.hasPermi('system:organizationinfo:edit')")
    @Log(title = "租户组织", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UumOrganizationinfo uumOrganizationinfo)
    {
        return toAjax(uumOrganizationinfoService.updateUumOrganizationinfo(uumOrganizationinfo));
    }

    /**
     * 删除租户组织
     */
    @PreAuthorize("@ss.hasPermi('system:organizationinfo:remove')")
    @Log(title = "租户组织", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ouguids}")
    public AjaxResult remove(@PathVariable String[] ouguids)
    {
        return toAjax(uumOrganizationinfoService.deleteUumOrganizationinfoByIds(ouguids));
    }

    @GetMapping("/treeselect")
    @ApiOperation(value = "获取部门下拉树列表")
    public AjaxResult treeselect(UumOrgInfo dept)
    {
        List<UumOrgInfo> depts = uumOrganizationinfoService.selectUumOrgInfoList(dept);
        List<OrgTreeSelect> orgTreeSelects = uumOrganizationinfoService.buildDeptTreeSelect(depts);
        return AjaxResult.success(orgTreeSelects);
    }
}
