package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.NgsStudent;
import com.yabushan.system.service.INgsStudentService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 学生Controller
 *
 * @author yabushan
 * @date 2021-06-05
 */
@RestController
@RequestMapping("/system/student")
public class NgsStudentController extends BaseController
{
    @Autowired
    private INgsStudentService ngsStudentService;

    /**
     * 查询学生列表
     */
    @PreAuthorize("@ss.hasPermi('system:student:list')")
    @GetMapping("/list")
    public TableDataInfo list(NgsStudent ngsStudent)
    {
        if("1".equals(ngsStudent.getStOrgId())){
            ngsStudent.setStOrgId(null);
        }
        startPage();
        List<NgsStudent> list = ngsStudentService.selectNgsStudentList(ngsStudent);
        return getDataTable(list);
    }

    /**
     * 导出学生列表
     */
    @PreAuthorize("@ss.hasPermi('system:student:export')")
    @Log(title = "学生", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(NgsStudent ngsStudent)
    {
        List<NgsStudent> list = ngsStudentService.selectNgsStudentList(ngsStudent);
        ExcelUtil<NgsStudent> util = new ExcelUtil<NgsStudent>(NgsStudent.class);
        return util.exportExcel(list, "student");
    }

    /**
     * 获取学生详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:student:query')")
    @GetMapping(value = "/{stId}")
    public AjaxResult getInfo(@PathVariable("stId") String stId)
    {
        return AjaxResult.success(ngsStudentService.selectNgsStudentById(stId));
    }

    /**
     * 新增学生
     */
    @PreAuthorize("@ss.hasPermi('system:student:add')")
    @Log(title = "学生", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NgsStudent ngsStudent)
    {
        return toAjax(ngsStudentService.insertNgsStudent(ngsStudent));
    }

    /**
     * 修改学生
     */
    @PreAuthorize("@ss.hasPermi('system:student:edit')")
    @Log(title = "学生", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NgsStudent ngsStudent)
    {
        return toAjax(ngsStudentService.updateNgsStudent(ngsStudent));
    }

    /**
     * 删除学生
     */
    @PreAuthorize("@ss.hasPermi('system:student:remove')")
    @Log(title = "学生", businessType = BusinessType.DELETE)
	@DeleteMapping("/{stIds}")
    public AjaxResult remove(@PathVariable String[] stIds)
    {
        return toAjax(ngsStudentService.deleteNgsStudentByIds(stIds));
    }
}
