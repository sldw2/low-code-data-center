package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxCustomerInformationInfo;
import com.yabushan.system.service.IYmxCustomerInformationInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 客户信息Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxinformationinfo")
public class YmxCustomerInformationInfoController extends BaseController
{
    @Autowired
    private IYmxCustomerInformationInfoService ymxCustomerInformationInfoService;

    /**
     * 查询客户信息列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxinformationinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxCustomerInformationInfo ymxCustomerInformationInfo)
    {
        startPage();
        List<YmxCustomerInformationInfo> list = ymxCustomerInformationInfoService.selectYmxCustomerInformationInfoList(ymxCustomerInformationInfo);
        return getDataTable(list);
    }

    /**
     * 导出客户信息列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxinformationinfo:export')")
    @Log(title = "客户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxCustomerInformationInfo ymxCustomerInformationInfo)
    {
        List<YmxCustomerInformationInfo> list = ymxCustomerInformationInfoService.selectYmxCustomerInformationInfoList(ymxCustomerInformationInfo);
        ExcelUtil<YmxCustomerInformationInfo> util = new ExcelUtil<YmxCustomerInformationInfo>(YmxCustomerInformationInfo.class);
        return util.exportExcel(list, "ymxinformationinfo");
    }

    /**
     * 获取客户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxinformationinfo:query')")
    @GetMapping(value = "/{customerInformationId}")
    public AjaxResult getInfo(@PathVariable("customerInformationId") String customerInformationId)
    {
        return AjaxResult.success(ymxCustomerInformationInfoService.selectYmxCustomerInformationInfoById(customerInformationId));
    }

    /**
     * 新增客户信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxinformationinfo:add')")
    @Log(title = "客户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxCustomerInformationInfo ymxCustomerInformationInfo)
    {
        return toAjax(ymxCustomerInformationInfoService.insertYmxCustomerInformationInfo(ymxCustomerInformationInfo));
    }

    /**
     * 修改客户信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxinformationinfo:edit')")
    @Log(title = "客户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxCustomerInformationInfo ymxCustomerInformationInfo)
    {
        return toAjax(ymxCustomerInformationInfoService.updateYmxCustomerInformationInfo(ymxCustomerInformationInfo));
    }

    /**
     * 删除客户信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxinformationinfo:remove')")
    @Log(title = "客户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{customerInformationIds}")
    public AjaxResult remove(@PathVariable String[] customerInformationIds)
    {
        return toAjax(ymxCustomerInformationInfoService.deleteYmxCustomerInformationInfoByIds(customerInformationIds));
    }
}
