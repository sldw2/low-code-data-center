package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubQuaCertify;
import com.yabushan.system.service.IEmpSubQuaCertifyService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工证书子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/certify")
public class EmpSubQuaCertifyController extends BaseController
{
    @Autowired
    private IEmpSubQuaCertifyService empSubQuaCertifyService;

    /**
     * 查询员工证书子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:certify:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubQuaCertify empSubQuaCertify)
    {
        startPage();
        List<EmpSubQuaCertify> list = empSubQuaCertifyService.selectEmpSubQuaCertifyList(empSubQuaCertify);
        return getDataTable(list);
    }

    /**
     * 导出员工证书子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:certify:export')")
    @Log(title = "员工证书子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubQuaCertify empSubQuaCertify)
    {
        List<EmpSubQuaCertify> list = empSubQuaCertifyService.selectEmpSubQuaCertifyList(empSubQuaCertify);
        ExcelUtil<EmpSubQuaCertify> util = new ExcelUtil<EmpSubQuaCertify>(EmpSubQuaCertify.class);
        return util.exportExcel(list, "certify");
    }

    /**
     * 获取员工证书子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:certify:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubQuaCertifyService.selectEmpSubQuaCertifyById(recId));
    }

    /**
     * 新增员工证书子集
     */
    @PreAuthorize("@ss.hasPermi('system:certify:add')")
    @Log(title = "员工证书子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubQuaCertify empSubQuaCertify)
    {
        return toAjax(empSubQuaCertifyService.insertEmpSubQuaCertify(empSubQuaCertify));
    }

    /**
     * 修改员工证书子集
     */
    @PreAuthorize("@ss.hasPermi('system:certify:edit')")
    @Log(title = "员工证书子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubQuaCertify empSubQuaCertify)
    {
        return toAjax(empSubQuaCertifyService.updateEmpSubQuaCertify(empSubQuaCertify));
    }

    /**
     * 删除员工证书子集
     */
    @PreAuthorize("@ss.hasPermi('system:certify:remove')")
    @Log(title = "员工证书子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubQuaCertifyService.deleteEmpSubQuaCertifyByIds(recIds));
    }
}
