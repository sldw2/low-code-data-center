package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxCustomerGiftInfo;
import com.yabushan.system.service.IYmxCustomerGiftInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 客户礼物Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxcustomergiftInfo")
public class YmxCustomerGiftInfoController extends BaseController
{
    @Autowired
    private IYmxCustomerGiftInfoService ymxCustomerGiftInfoService;

    /**
     * 查询客户礼物列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcustomergiftInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxCustomerGiftInfo ymxCustomerGiftInfo)
    {
        startPage();
        List<YmxCustomerGiftInfo> list = ymxCustomerGiftInfoService.selectYmxCustomerGiftInfoList(ymxCustomerGiftInfo);
        return getDataTable(list);
    }

    /**
     * 导出客户礼物列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcustomergiftInfo:export')")
    @Log(title = "客户礼物", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxCustomerGiftInfo ymxCustomerGiftInfo)
    {
        List<YmxCustomerGiftInfo> list = ymxCustomerGiftInfoService.selectYmxCustomerGiftInfoList(ymxCustomerGiftInfo);
        ExcelUtil<YmxCustomerGiftInfo> util = new ExcelUtil<YmxCustomerGiftInfo>(YmxCustomerGiftInfo.class);
        return util.exportExcel(list, "ymxcustomergiftInfo");
    }

    /**
     * 获取客户礼物详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcustomergiftInfo:query')")
    @GetMapping(value = "/{customerGiftId}")
    public AjaxResult getInfo(@PathVariable("customerGiftId") String customerGiftId)
    {
        return AjaxResult.success(ymxCustomerGiftInfoService.selectYmxCustomerGiftInfoById(customerGiftId));
    }

    /**
     * 新增客户礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcustomergiftInfo:add')")
    @Log(title = "客户礼物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxCustomerGiftInfo ymxCustomerGiftInfo)
    {
        return toAjax(ymxCustomerGiftInfoService.insertYmxCustomerGiftInfo(ymxCustomerGiftInfo));
    }

    /**
     * 修改客户礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcustomergiftInfo:edit')")
    @Log(title = "客户礼物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxCustomerGiftInfo ymxCustomerGiftInfo)
    {
        return toAjax(ymxCustomerGiftInfoService.updateYmxCustomerGiftInfo(ymxCustomerGiftInfo));
    }

    /**
     * 删除客户礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcustomergiftInfo:remove')")
    @Log(title = "客户礼物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{customerGiftIds}")
    public AjaxResult remove(@PathVariable String[] customerGiftIds)
    {
        return toAjax(ymxCustomerGiftInfoService.deleteYmxCustomerGiftInfoByIds(customerGiftIds));
    }
}
