package com.yabushan.form.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.form.domain.JimuReportDb;
import com.yabushan.form.service.IJimuReportDbService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 报表APIController
 *
 * @author yabushan
 * @date 2021-07-03
 */
@RestController
@RequestMapping("/form/reportdb")
public class JimuReportDbController extends BaseController
{
    @Autowired
    private IJimuReportDbService jimuReportDbService;

    /**
     * 查询报表API列表
     */
    @PreAuthorize("@ss.hasPermi('form:reportdb:list')")
    @GetMapping("/list")
    public TableDataInfo list(JimuReportDb jimuReportDb)
    {
        startPage();
        List<JimuReportDb> list = jimuReportDbService.selectJimuReportDbList(jimuReportDb);
        return getDataTable(list);
    }

    /**
     * 导出报表API列表
     */
    @PreAuthorize("@ss.hasPermi('form:reportdb:export')")
    @Log(title = "报表API", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(JimuReportDb jimuReportDb)
    {
        List<JimuReportDb> list = jimuReportDbService.selectJimuReportDbList(jimuReportDb);
        ExcelUtil<JimuReportDb> util = new ExcelUtil<JimuReportDb>(JimuReportDb.class);
        return util.exportExcel(list, "reportdb");
    }

    /**
     * 获取报表API详细信息
     */
    @PreAuthorize("@ss.hasPermi('form:reportdb:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(jimuReportDbService.selectJimuReportDbById(id));
    }

    /**
     * 新增报表API
     */
    @PreAuthorize("@ss.hasPermi('form:reportdb:add')")
    @Log(title = "报表API", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody JimuReportDb jimuReportDb)
    {
        return toAjax(jimuReportDbService.insertJimuReportDb(jimuReportDb));
    }

    /**
     * 修改报表API
     */
    @PreAuthorize("@ss.hasPermi('form:reportdb:edit')")
    @Log(title = "报表API", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody JimuReportDb jimuReportDb)
    {
        return toAjax(jimuReportDbService.updateJimuReportDb(jimuReportDb));
    }

    /**
     * 删除报表API
     */
    @PreAuthorize("@ss.hasPermi('form:reportdb:remove')")
    @Log(title = "报表API", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(jimuReportDbService.deleteJimuReportDbByIds(ids));
    }
}
