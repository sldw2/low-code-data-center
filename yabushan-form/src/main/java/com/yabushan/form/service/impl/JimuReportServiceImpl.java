package com.yabushan.form.service.impl;

import java.util.List;
import com.yabushan.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.form.mapper.JimuReportMapper;
import com.yabushan.form.domain.JimuReport;
import com.yabushan.form.service.IJimuReportService;

/**
 * 在线excel设计器Service业务层处理
 *
 * @author yabushan
 * @date 2021-07-03
 */
@Service("yabushanreport")
public class JimuReportServiceImpl implements IJimuReportService
{
    @Autowired
    private JimuReportMapper jimuReportMapper;

    /**
     * 查询在线excel设计器
     *
     * @param id 在线excel设计器ID
     * @return 在线excel设计器
     */
    @Override
    public JimuReport selectJimuReportById(String id)
    {
        return jimuReportMapper.selectJimuReportById(id);
    }

    /**
     * 查询在线excel设计器列表
     *
     * @param jimuReport 在线excel设计器
     * @return 在线excel设计器
     */
    @Override
    public List<JimuReport> selectJimuReportList(JimuReport jimuReport)
    {
        return jimuReportMapper.selectJimuReportList(jimuReport);
    }

    /**
     * 新增在线excel设计器
     *
     * @param jimuReport 在线excel设计器
     * @return 结果
     */
    @Override
    public int insertJimuReport(JimuReport jimuReport)
    {
        jimuReport.setCreateTime(DateUtils.getNowDate());
        return jimuReportMapper.insertJimuReport(jimuReport);
    }

    /**
     * 修改在线excel设计器
     *
     * @param jimuReport 在线excel设计器
     * @return 结果
     */
    @Override
    public int updateJimuReport(JimuReport jimuReport)
    {
        jimuReport.setUpdateTime(DateUtils.getNowDate());
        return jimuReportMapper.updateJimuReport(jimuReport);
    }

    /**
     * 批量删除在线excel设计器
     *
     * @param ids 需要删除的在线excel设计器ID
     * @return 结果
     */
    @Override
    public int deleteJimuReportByIds(String[] ids)
    {
        return jimuReportMapper.deleteJimuReportByIds(ids);
    }

    /**
     * 删除在线excel设计器信息
     *
     * @param id 在线excel设计器ID
     * @return 结果
     */
    @Override
    public int deleteJimuReportById(String id)
    {
        return jimuReportMapper.deleteJimuReportById(id);
    }
}
