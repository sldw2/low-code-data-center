package com.yabushan.form.domain;

import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 报表API对象 jimu_report_db
 *
 * @author yabushan
 * @date 2021-07-03
 */
public class JimuReportDb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 主键字段 */
    @Excel(name = "主键字段")
    private String jimuReportId;

    /** 数据集编码 */
    @Excel(name = "数据集编码")
    private String dbCode;

    /** 数据集名字 */
    @Excel(name = "数据集名字")
    private String dbChName;

    /** 数据源类型 */
    @Excel(name = "数据源类型")
    private String dbType;

    /** 数据库表名 */
    @Excel(name = "数据库表名")
    private String dbTableName;

    /** 动态查询SQL */
    @Excel(name = "动态查询SQL")
    private String dbDynSql;

    /** 数据源KEY */
    @Excel(name = "数据源KEY")
    private String dbKey;

    /** 填报数据源 */
    @Excel(name = "填报数据源")
    private String tbDbKey;

    /** 填报数据表 */
    @Excel(name = "填报数据表")
    private String tbDbTableName;

    /** java类数据集  类型（spring:springkey,class:java类名） */
    @Excel(name = "java类数据集  类型", readConverterExp = "s=pring:springkey,class:java类名")
    private String javaType;

    /** java类数据源  数值（bean key/java类名） */
    @Excel(name = "java类数据源  数值", readConverterExp = "b=ean,k=ey/java类名")
    private String javaValue;

    /** 请求地址 */
    @Excel(name = "请求地址")
    private String apiUrl;

    /** 请求方法0-get,1-post */
    @Excel(name = "请求方法0-get,1-post")
    private String apiMethod;

    /** 是否是列表0否1是 默认0 */
    @Excel(name = "是否是列表0否1是 默认0")
    private Integer isList;

    /** 是否作为分页,0:不分页，1:分页 */
    @Excel(name = "是否作为分页,0:不分页，1:分页")
    private String isPage;

    /** 数据源 */
    @Excel(name = "数据源")
    private String dbSource;

    /** 数据库类型 MYSQL ORACLE SQLSERVER */
    @Excel(name = "数据库类型 MYSQL ORACLE SQLSERVER")
    private String dbSourceType;

    /** json数据，直接解析json内容 */
    @Excel(name = "json数据，直接解析json内容")
    private String jsonData;

    /** api转换器 */
    @Excel(name = "api转换器")
    private String apiConvert;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setJimuReportId(String jimuReportId)
    {
        this.jimuReportId = jimuReportId;
    }

    public String getJimuReportId()
    {
        return jimuReportId;
    }
    public void setDbCode(String dbCode)
    {
        this.dbCode = dbCode;
    }

    public String getDbCode()
    {
        return dbCode;
    }
    public void setDbChName(String dbChName)
    {
        this.dbChName = dbChName;
    }

    public String getDbChName()
    {
        return dbChName;
    }
    public void setDbType(String dbType)
    {
        this.dbType = dbType;
    }

    public String getDbType()
    {
        return dbType;
    }
    public void setDbTableName(String dbTableName)
    {
        this.dbTableName = dbTableName;
    }

    public String getDbTableName()
    {
        return dbTableName;
    }
    public void setDbDynSql(String dbDynSql)
    {
        this.dbDynSql = dbDynSql;
    }

    public String getDbDynSql()
    {
        return dbDynSql;
    }
    public void setDbKey(String dbKey)
    {
        this.dbKey = dbKey;
    }

    public String getDbKey()
    {
        return dbKey;
    }
    public void setTbDbKey(String tbDbKey)
    {
        this.tbDbKey = tbDbKey;
    }

    public String getTbDbKey()
    {
        return tbDbKey;
    }
    public void setTbDbTableName(String tbDbTableName)
    {
        this.tbDbTableName = tbDbTableName;
    }

    public String getTbDbTableName()
    {
        return tbDbTableName;
    }
    public void setJavaType(String javaType)
    {
        this.javaType = javaType;
    }

    public String getJavaType()
    {
        return javaType;
    }
    public void setJavaValue(String javaValue)
    {
        this.javaValue = javaValue;
    }

    public String getJavaValue()
    {
        return javaValue;
    }
    public void setApiUrl(String apiUrl)
    {
        this.apiUrl = apiUrl;
    }

    public String getApiUrl()
    {
        return apiUrl;
    }
    public void setApiMethod(String apiMethod)
    {
        this.apiMethod = apiMethod;
    }

    public String getApiMethod()
    {
        return apiMethod;
    }
    public void setIsList(Integer isList)
    {
        this.isList = isList;
    }

    public Integer getIsList()
    {
        return isList;
    }
    public void setIsPage(String isPage)
    {
        this.isPage = isPage;
    }

    public String getIsPage()
    {
        return isPage;
    }
    public void setDbSource(String dbSource)
    {
        this.dbSource = dbSource;
    }

    public String getDbSource()
    {
        return dbSource;
    }
    public void setDbSourceType(String dbSourceType)
    {
        this.dbSourceType = dbSourceType;
    }

    public String getDbSourceType()
    {
        return dbSourceType;
    }
    public void setJsonData(String jsonData)
    {
        this.jsonData = jsonData;
    }

    public String getJsonData()
    {
        return jsonData;
    }
    public void setApiConvert(String apiConvert)
    {
        this.apiConvert = apiConvert;
    }

    public String getApiConvert()
    {
        return apiConvert;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jimuReportId", getJimuReportId())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("dbCode", getDbCode())
            .append("dbChName", getDbChName())
            .append("dbType", getDbType())
            .append("dbTableName", getDbTableName())
            .append("dbDynSql", getDbDynSql())
            .append("dbKey", getDbKey())
            .append("tbDbKey", getTbDbKey())
            .append("tbDbTableName", getTbDbTableName())
            .append("javaType", getJavaType())
            .append("javaValue", getJavaValue())
            .append("apiUrl", getApiUrl())
            .append("apiMethod", getApiMethod())
            .append("isList", getIsList())
            .append("isPage", getIsPage())
            .append("dbSource", getDbSource())
            .append("dbSourceType", getDbSourceType())
            .append("jsonData", getJsonData())
            .append("apiConvert", getApiConvert())
            .toString();
    }
}
